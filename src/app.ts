import { TaskManager } from "./core/task-manager";
import { TestingGetUserApi } from "./tasks/testing-get-user-api.task";
import { DeleteTeamRoomsTask } from './tasks/delete-team-rooms.task';

let taskManager = new TaskManager();
// taskManager.register(new TestingGetUserApi());
taskManager.register(new DeleteTeamRoomsTask());
taskManager.start();