import { Task } from "../core/task-manager";
import { UserSession } from "../core/user-session";
import { RenewTokenInterceptor } from "../network/interceptor/renew-token-interceptor";
import { RoomResource } from "../network/room.resource";
import { Room } from "../network/room.model";


interface RoomContainer {
    roomNameMatch: RegExp;
    teamId: string;
}

export class DeleteTeamRoomsTask extends Task {
    
    public name: String = 'DeleteTeamRoomsTask';
    private session: UserSession;
    private interceptor: RenewTokenInterceptor;
    private roomResource: RoomResource;
    private itemsToDelete = new Array<RoomContainer>();

    constructor() {
        super();
        this.session = new UserSession();
        this.interceptor = new RenewTokenInterceptor(this.session);
        this.initItemsToDelete();
    }

    public run(onComplete: (task: Task) => void, onError: (task: Task, reason: any) => void) {
        let self = this;
        this.session.start('w145214@mvrht.net', 'asdf1234').then(() => {
            self.roomResource = new RoomResource(this.interceptor);
            self.onSessionReady(onComplete, onError);
        })
        .catch((err) => {
            console.log('login failed');
            console.log(err);
            onError(self, err);
        });
    }

    private onSessionReady(onComplete: (task: Task) => void, onError: (task: Task, reason: any) => void) {

        if ( this.itemsToDelete.length <= 0 ) {
            onComplete(this);
            return;
        }
        
        let self = this;
        let deletePromises = new Array<Promise<any>>();
        for ( let item of this.itemsToDelete ) {
            let prom = new Promise((resolve, reject) => {
                self.deleteItem(item, () => {
                    resolve();
                }, (err) => {
                    reject(err);
                })
            });
            deletePromises.push(prom);
        }
        
        Promise.all(deletePromises)
        .then(() => {
            onComplete(self);
        })
        .catch((reason) => {
            onError(self, reason);
        })
    }

    private deleteItem(item: RoomContainer, onComplete: Function, onError: (reason: any) => void) {
        let self = this;
        this.roomResource.getRooms(item.teamId)
        .then((data) => {
            let matchedRooms = self.foundMatchedRooms(item, data.items);
            let deletePromises = new Array<Promise<any>>();
            if ( matchedRooms.length <= 0 ) {
                onComplete();
                return;
            }
            for ( let room of matchedRooms ) {
                console.log('delele room; team: ' + item.teamId + ', room: ' + room.id + '');
                deletePromises.push(self.roomResource.deleteRoom(item.teamId, room.id));
            }
            Promise.all(deletePromises)
            .then(() => {
                onComplete();
            })
            .catch((reason) => {
                onError(reason);
            })
        })
        .catch((err) => {
            onError(err);
        });
    }

    private initItemsToDelete() {

        // TEAM: Live taggin tests

        this.itemsToDelete.push({
            teamId: '5b053aa0d852fc2260417d20',
            roomNameMatch: RegExp('Room for testing Tag sets view')
        });
        
        // Team: video edit tests

        this.itemsToDelete.push({
            teamId: '5a6aebb13c48f32df8c89e2c',
            roomNameMatch: RegExp('Android AutoTest')
        });

        this.itemsToDelete.push({
            teamId: '5a6aebb13c48f32df8c89e2c',
            roomNameMatch: RegExp('iPhoneTest')
        });

        this.itemsToDelete.push({
            teamId: '5a6aebb13c48f32df8c89e2c',
            roomNameMatch: RegExp('iPadTest')
        });

        // TEAM: Tags tests

        this.itemsToDelete.push({
            teamId: '5a7173f93c48f352f83fee1c',
            roomNameMatch: RegExp('draft tests room')
        });

        this.itemsToDelete.push({
            teamId: '5a7173f93c48f352f83fee1c',
            roomNameMatch: RegExp('iPhoneTest')
        });

        this.itemsToDelete.push({
            teamId: '5a7173f93c48f352f83fee1c',
            roomNameMatch: RegExp('iPadTest')
        });

        this.itemsToDelete.push({
            teamId: '5a7173f93c48f352f83fee1c',
            roomNameMatch: RegExp('Android AutoTest')
        });
    }

    private foundMatchedRooms(item: RoomContainer, rooms: Array<Room>) {
        return rooms.filter(v => v.name.match(item.roomNameMatch) !== null )
    }
}
