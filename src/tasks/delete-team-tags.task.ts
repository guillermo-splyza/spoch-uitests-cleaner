import { UserSession } from "../core/user-session";
import { RenewTokenInterceptor } from "../network/interceptor/renew-token-interceptor";
import { Task } from "../core/task-manager";
import { TeamGroupResource } from "../network/team-group.resource";
import { TeamGroup } from "../network/team-group.model";

interface TagContainer {
    groupNameMatch: RegExp;
    teamId: string;
}

export class DeleteTeamTagsTask extends Task {
    
    public name: String = 'DeleteTeamRoomsTask';
    private session: UserSession;
    private interceptor: RenewTokenInterceptor;
    private teamGroupResource: TeamGroupResource;
    private teamsIds = new Array<TagContainer>();

    constructor() {
        super();
        this.session = new UserSession();
        this.interceptor = new RenewTokenInterceptor(this.session);
        this.initItemsToDelete();
    }

    public run(onComplete: (task: Task) => void, onError: (task: Task, reason: any) => void) {
        let self = this;
        this.session.start('w145214@mvrht.net', 'asdf1234').then(() => {
            self.teamGroupResource = new TeamGroupResource(this.interceptor);
            self.onSessionReady(onComplete, onError);
        })
        .catch((err) => {
            console.log('login failed');
            console.log(err);
            onError(self, err);
        });
    }

    private onSessionReady(onComplete: (task: Task) => void, onError: (task: Task, reason: any) => void) {

        if ( this.teamsIds.length <= 0 ) {
            onComplete(this);
            return;
        }
        
        let self = this;
        let deletePromises = new Array<Promise<any>>();
        for ( let item of this.teamsIds ) {
            let prom = new Promise((resolve, reject) => {
                self.deleteItem(item, () => {
                    resolve();
                }, (err) => {
                    reject(err);
                })
            });
            deletePromises.push(prom);
        }
        
        Promise.all(deletePromises)
        .then(() => {
            onComplete(self);
        })
        .catch((reason) => {
            onError(self, reason);
        })
    }

    private deleteItem(item: TagContainer, onComplete: Function, onError: (reason: any) => void) {
        let self = this;
        this.teamGroupResource.getTeamGroups(item.teamId)
        .then((data) => {
            let groups = self.foundMatchedRooms(item, data.items);
            let deletePromises = new Array<Promise<any>>();
            if ( groups.length <= 0 ) {
                onComplete();
                return;
            }
            for ( let group of groups ) {
                console.log('delele team group; team: ' + group + ', group: ' + group.id + ', ' + group.name);
                deletePromises.push(self.teamGroupResource.deleteGroup(item.teamId, group.id));
            }
            Promise.all(deletePromises)
            .then(() => {
                onComplete();
            })
            .catch((reason) => {
                onError(reason);
            })
        })
        .catch((err) => {
            onError(err);
        });
    }

    private foundMatchedRooms(item: TagContainer, rooms: Array<TeamGroup>) {
        return rooms.filter(v => v.name.match(item.groupNameMatch) !== null )
    }

    private initItemsToDelete() {

        // TEAM: Live taggin tests

        this.teamsIds.push({
            teamId: '5b053aa0d852fc2260417d20',
            groupNameMatch: RegExp('Mikan:')
        });
        
    }

}
