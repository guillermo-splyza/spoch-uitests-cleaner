import { Task } from "../core/task-manager";
import { UserSession } from "../core/user-session";
import { UserResource } from "../network/user.resource";
import { RenewTokenInterceptor } from "../network/interceptor/renew-token-interceptor";


export class TestingGetUserApi extends Task {
    
    public name: String = 'TestingGetUserApi';
    private session: UserSession;
    private interceptor: RenewTokenInterceptor;

    constructor() {
        super();
        this.session = new UserSession();
        this.interceptor = new RenewTokenInterceptor(this.session);
    }

    public run(onComplete: (task: Task) => void, onError: (task: Task, reason: any) => void) {
        let self = this;
        this.session.start('testy.mcuser@splyza.com', 'asdf1234').then(() => {
            self.onSessionReady(onComplete, onError);
        })
        .catch((err) => {
            console.log('login failed');
            console.log(err);
            onError(self, err);
        });
    }

    private onSessionReady(onComplete: (task: Task) => void, onError: (task: Task, reason: any) => void) {
        console.log('login completed');
        let self = this;
        let userResource = new UserResource(self.interceptor);
        let prom1 = userResource.getSelfInfo().then((resp) => {
            console.log('get user info ok');
            console.log(resp.name);
        })
        .catch((err) => {
            console.log('cannot get user info');
            console.log(err);
        });

        /// second call
        let prom2 = userResource.getSelfInfo().then((resp) => {
            console.log('get user info ok 2');
            console.log(resp.name);
        })
        .catch((err) => {
            console.log('cannot get user info 2');
            console.log(err);
        });

        Promise.all([prom1, prom2])
        .then(() => {
            onComplete(self);
        })
        .catch((err) => {
            onError(self, err);
        });
    }
}