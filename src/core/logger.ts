
import * as Winston from 'winston';
import * as path from 'path';
import * as fs from 'fs';

let fileName = path.join(__dirname, '/../logs.log');
try {
    fs.unlinkSync(fileName);
} catch (err) {}

const logger = Winston.createLogger({
    level: 'info',
    format: Winston.format.simple(),
    transports: [
        new Winston.transports.File({filename: fileName, level: 'info' }),
    ]
  });


export function ConsoleLog(...args: any[]) {
    let text = '';
    for ( let arg of args) {
        text += arg;
    }
    let cleannedText = text.replace(/\[32m/g, '').replace(/\[39m/g, '');
    console.log(text);
    logger.log({
        level: 'info',
        message: cleannedText
    });
}
