import { AuthResourceService } from "../network/auth/auth.resource.service";
import { AuthToken } from "../network/auth/auth-token.model";
import { TokenRenewer } from "../network/auth/auth-renewer";

export class UserSession {

    public get token() { return this._token; }
    private tokenRenewer = new TokenRenewer();
    private _token: AuthToken;

    public start(userName: string, password: string): Promise<void> {
        let self = this;
        return AuthResourceService.shared.login(userName, password)
        .then((token) => {
            this._token = token;
        });
    }

    public renewToken(): Promise<AuthToken>{
        let self = this;
        return this.tokenRenewer.renewAccessToken(this.token)
        .then((newToken) => {
            self._token = newToken;
            return newToken;
        });
    }
}