import chalk from 'chalk';
import { ConsoleLog } from './logger';


// TODO: Add maxExecution time checker

export abstract class Task {
    public isCompleted = false;
    public abstract name: String;
    public abstract run(onComplete: (task: Task) => void, onError: (task: Task, reason: any) => void);
}

export class TaskManager {
    
    public tasks = new Array<Task>();
    private currentIndex = 0;
    private isRunning = false;

    public register(newTask: Task) {
        this.tasks.push(newTask);
    }

    public start() {
    
        if ( this.isRunning ) { return; }
        if ( this.tasks.length <= 0 ) { 
            this.onComplete();
            return;
        }

        this.currentIndex = 0;
        this.isRunning = true;

        let self = this;

        function onRunTaskDone() {
            let nextTask = self.getNextTask();
            if ( nextTask ) {
                self.runTask(nextTask, onRunTaskDone);
            } else {
                self.onComplete();
            }
        };

        this.runTask(this.getNextTask(), onRunTaskDone);
        
    }

    private getNextTask(): Task | null {
        let currentTask = this.tasks[this.currentIndex];
        this.currentIndex++;
        return currentTask;
    }

    private runTask(task: Task, done: () => void) {
        ConsoleLog(chalk.green('Running task: ')  + task.name);
        task.run((runningTask) => {
            ConsoleLog(chalk.green('Task ') + runningTask.name + chalk.green(' completed'));
            done();
        }, (task, reason) => {
            ConsoleLog(chalk.red('task failed: ' + task.name + ' , '), reason);
            done();
        });
    }

    private onComplete() {
        this.isRunning = false;
        this.currentIndex = 0;
        ConsoleLog(chalk.green('Task manager has finished. '));
        ConsoleLog(chalk.green(this.tasks.length + ' task(s) where executed.'));
    }
}