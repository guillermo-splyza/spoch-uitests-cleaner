import Environment from '../environment.config';
import { SpochRequest } from './spoch-request';
import { RenewTokenInterceptor } from './interceptor/renew-token-interceptor';
import { IResourceCollection } from './resource-collection.model';
import { Room } from './room.model';


export class RoomResource {

    private env = Environment;

    constructor(private interceptor: RenewTokenInterceptor) {}

    public getRooms(teamId: string): Promise<IResourceCollection<Room>> {

        const _url =  '/teams/:teamId/rooms';
        const url = this.env.apiBase + _url.replace(/:teamId/, teamId);

        const options  = {
            json: true
        };

        return SpochRequest.httpGet(url, options, this.interceptor);
    }

    public deleteRoom(teamId: string, roomId: string): Promise<any> {
        const _url =  '/teams/:teamId/rooms/:roomId';
        const url = this.env.apiBase + _url.replace(/:teamId/, teamId).replace(/:roomId/, roomId);

        const options  = {
            json: true
        };

        return SpochRequest.httpDelete(url, options, this.interceptor);
    }
}
