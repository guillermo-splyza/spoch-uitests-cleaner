import Environment from '../environment.config';
import { SpochRequest } from './spoch-request';
import { RenewTokenInterceptor } from './interceptor/renew-token-interceptor';
import { User } from './user.model';

export class UserResource {

    private env = Environment;

    constructor(private interceptor: RenewTokenInterceptor) {}

    public getSelfInfo(): Promise<User> {

        const url = this.env.apiBase + '/users/self';

        const options  = {
            json: true
        };

        return SpochRequest.httpGet(url, options, this.interceptor);
    }
}
