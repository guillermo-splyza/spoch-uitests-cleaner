import * as request from 'request';
import { SpochRequestInterceptor } from './interceptor/spoch-request-interceptor';

export interface SpochRequestResp {
    statusCode: number;
    body: any;
    statusMessage: string;
    error?: any;
}

export class SpochRequest {

    public static httpGet<T>(url: string, options: request.CoreOptions, interceptor = new SpochRequestInterceptor()): Promise<T> {
        let self = this;
        let promise = new Promise<T>((resolve, reject) => {
            interceptor.execute(url, options, request.get)
            .then((requestData) => {
                self.thenHandler(requestData.error, requestData.response, requestData.body, resolve, reject);
            })
            .catch((err) => {
                reject(err);
            });
        });
        return promise;
    }

    public static httpDelete<T>(url: string, options: request.CoreOptions, interceptor = new SpochRequestInterceptor()): Promise<T> {
        let self = this;
        let promise = new Promise<T>((resolve, reject) => {
            interceptor.execute(url, options, request.delete)
            .then((requestData) => {
                self.thenHandler(requestData.error, requestData.response, requestData.body, resolve, reject);
            })
            .catch((err) => {
                reject(err);
            });
        });
        return promise;
    }

    private static thenHandler<T>(
        error: any, 
        res: request.Response, 
        body: any, 
        resolve:  (value?: T | PromiseLike<T>) => void, 
        reject: (reason?: any) => void 
    ) {
        if ( error ) {
            reject({
                statusCode: res.statusCode,
                body: res.body,
                statusMessage: res.statusMessage,
                error: error
            });
        } else if ( res.statusCode < 200 || res.statusCode >= 400 ) {
            reject({
                statusCode: res.statusCode,
                body: res.body,
                statusMessage: res.statusMessage
            });
        } else  {
            resolve(body);
        }
    }

}