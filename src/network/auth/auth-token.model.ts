export class AuthToken {
    public access_token = '';
    public refresh_token = '';
    public expires_in = 0;
    public token_type = '';
    public expires = '';
    public issued = '';
}