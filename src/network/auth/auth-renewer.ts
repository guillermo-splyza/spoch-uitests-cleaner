import { AuthResourceService } from "./auth.resource.service";
import { AuthToken } from "./auth-token.model";

type Resolve = (value?: AuthToken | PromiseLike<AuthToken>) => void;
type Reject = (reason?: any) => void;

export class TokenRenewer {

    private readonly MaxTries = 5;
    private pendingRequest: Promise<AuthToken>;
    private tries = 0;

    public renewAccessToken(authToken: AuthToken):  Promise<AuthToken> {
        return this._renewAccessToken(authToken);
    }

    private _renewAccessToken(authToken: AuthToken):  Promise<AuthToken> {
        let self = this;
        if ( this.pendingRequest ) { 
            return this.pendingRequest; 
        }

        let doOnSuccess = function(newToken: AuthToken, resolve: Resolve) {
            resolve(newToken);
            self.tries = 0;
            delete self.pendingRequest;
        }

        let doOnError = function(errResp: any, reject: Reject, resolve: Resolve) {
            let shouldReject = false;
            if ( errResp.statusCode !== 401 ) { shouldReject = true; }
            if ( self.tries > self.MaxTries) { shouldReject = true; }
            if ( shouldReject ) {
                reject(errResp);
                self.tries = 0;
                delete self.pendingRequest;
            } else {
                self.tries += 1;
                setTimeout(() => {
                    self.tryToRenew(authToken.refresh_token, function(newToken) {
                        doOnSuccess(newToken, resolve);
                    }, function(resp) {
                        doOnError(resp, reject, resolve);
                    });
                }, 300);
            }
        };
        
        this.pendingRequest = new Promise<AuthToken>((resolve, reject) => {
            self.tryToRenew(authToken.refresh_token, (newToken) => {
                doOnSuccess(newToken, resolve);
            }, (errResp) => {
                doOnError(errResp, reject, resolve);
            });

        });
        
        return this.pendingRequest;
    }

    private tryToRenew(renewToken: string, onSuccess: (token: AuthToken) => void, onError: (response: any) => void) {
        AuthResourceService.shared.renewAccessToken(renewToken)
        .then((newToken: AuthToken) => {
            onSuccess(newToken);
        })
        .catch((errResp) => {
            onError(errResp);
        });
    }
}