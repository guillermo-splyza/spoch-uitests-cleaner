import Environment from '../../environment.config';
import { AuthToken } from './auth-token.model';
import * as request from 'request';


export class AuthResourceService {

    public static shared = new AuthResourceService();
    private env = Environment;

    public login(email: string, password: string): Promise<AuthToken> {

        const url = this.env.apiHost + '/oauth/token';

        const data = {
            grant_type: 'password',
            password: password,
            username: email
        };

        let promise = new Promise<AuthToken>((resolve, reject) => {
            request.post(url, {json: true, form: data}, (err, res, body) => {
                if ( err ) {
                    reject({
                        statusCode: res.statusCode,
                        body: res.body,
                        statusMessage: res.statusMessage,
                        error: err
                    });
                } else if ( res.statusCode < 200 || res.statusCode >= 400 ) {
                    reject({
                        statusCode: res.statusCode,
                        body: res.body,
                        statusMessage: res.statusMessage
                    });
                } else  {
                    resolve(body);
                }
            });
        });

        return promise;
    }

    public renewAccessToken(refreshToken: string): Promise<AuthToken> {

        let url = this.env.apiHost + '/oauth/token';

        let data = {
            grant_type: 'refresh_token',
            refresh_token: refreshToken
        };

        let self = this;
        let promise  =  new Promise<AuthToken>((resolve, reject) => {
            request.post(url, {json: true, form: data}, (err, res, body) => {
                if ( err ) {
                    reject({
                        statusCode: res.statusCode,
                        body: res.body,
                        statusMessage: res.statusMessage,
                        error: err
                    });
                } else if ( res.statusCode < 200 || res.statusCode >= 400 ) {
                    reject({
                        statusCode: res.statusCode,
                        body: res.body,
                        statusMessage: res.statusMessage
                    });
                } else  {
                    resolve(body);
                }
            });
        });

        return promise;
    }

}