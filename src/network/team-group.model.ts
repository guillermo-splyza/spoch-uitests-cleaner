

export class TeamGroup {
    public id: string;
    public name: string;
    public isDefault: boolean;
    public updated: string;
    public type: string;
    public color: number;
}
