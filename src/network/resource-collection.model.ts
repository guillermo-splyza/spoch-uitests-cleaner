import { Link } from "./link.model";

export interface IResourceCollection<T> {
    items: T[];
    links?: Array<Link>;
    total?: number;
}
