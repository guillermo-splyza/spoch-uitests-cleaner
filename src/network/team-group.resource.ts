import Environment from '../environment.config';
import { SpochRequest } from './spoch-request';
import { RenewTokenInterceptor } from './interceptor/renew-token-interceptor';
import { IResourceCollection } from './resource-collection.model';
import { TeamGroup } from './team-group.model';


export class TeamGroupResource {

    private env = Environment;

    constructor(private interceptor: RenewTokenInterceptor) {}

    public getTeamGroups(teamId: string): Promise<IResourceCollection<TeamGroup>> {

        const _url =  '/teams/:teamId/groups';
        const url = this.env.apiBase + _url.replace(/:teamId/, teamId);

        const options  = {
            json: true
        };

        return SpochRequest.httpGet(url, options, this.interceptor);
    }

    public deleteGroup(teamId: string, groupId: string): Promise<any> {
        const _url = '/teams/:teamId/groups/:groupId';
        const url = this.env.apiBase + _url.replace(/:teamId/, teamId).replace(/:groupId/, groupId);

        const options  = {
            json: true
        };

        return SpochRequest.httpDelete(url, options, this.interceptor);
    }
}