import * as request from 'request';

export interface RequestInterceptorParams { 
    error: any;
    response: request.Response;
    body: any;

}

export class SpochRequestInterceptor {

    public execute(url: string, options: request.CoreOptions, requestMethod: (uri: string, options?: request.CoreOptions, callback?: request.RequestCallback) => request.Request): Promise<RequestInterceptorParams> {
        let promise = new Promise<RequestInterceptorParams>((resolve, reject) => {
            requestMethod(url, options, (err, res, body) => {
                resolve({
                    error: err,
                    response: res,
                    body: body
                });
            });
        });
        return promise;
    }
    
}
