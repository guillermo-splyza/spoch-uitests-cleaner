import * as request from 'request';
import { SpochRequestInterceptor, RequestInterceptorParams } from './spoch-request-interceptor';
import { UserSession } from '../../core/user-session';

export class RenewTokenInterceptor extends SpochRequestInterceptor {
    
    constructor(private session: UserSession) {
        super();
    }

    public execute(url: string, options: request.CoreOptions, requestMethod: (uri: string, options?: request.CoreOptions, callback?: request.RequestCallback) => request.Request): Promise<RequestInterceptorParams> {
        let self = this;
        this.insertAuthTokenOnHeaders(options, this.session);
        let promise = new Promise<RequestInterceptorParams>((resolve, reject) => {
            requestMethod(url, options, (err, response, body) => {
                self.intercept(response)
                .then(() => {
                    self.insertAuthTokenOnHeaders(options, self.session);
                    requestMethod(url, options, (err, res, body) => {
                        resolve({
                            error: err,
                            response: res,
                            body: body
                        });
                    });
                })
                .catch((reasons) => {
                    reject(reasons);
                });
            });
        });
        return promise;
    }

    private insertAuthTokenOnHeaders(options: request.CoreOptions, userSession: UserSession) {
        if ( !options.headers ) {  
            options.headers = {};
        }
        options.headers['Authorization'] = 'bearer ' + userSession.token.access_token;
    }

    private intercept(response: request.Response): Promise<void> {
        let self = this;
        let promise = new Promise<any>((resolve, reject) => {
            if ( response.statusCode === 401 ) {
                self.session.renewToken()
                .then((newToken) => {
                    resolve();
                })
                .catch((err) => {
                    reject(err);
                });
            } else {
                resolve();
            }
        });
        return promise;
    }
}
