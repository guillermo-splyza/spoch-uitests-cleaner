import { User } from "./user.model";
import { Link } from "./link.model";
import { Team } from "./team.model";

export class Room {
    public id: string;
    public type: string;
    public name: string;
    public details: string;
    // public picture: Picture;
    public updated: string;
    public created: string;
    public owner: Team;
    public author: User;
    public links?: Array<Link>;

    constructor(author?: User) {
        this.type = 'Room';
        this.name = '';
        this.details = '';
        if ( author ) {  this.author = author; }
    }
}
